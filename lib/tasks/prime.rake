namespace :dev do
  desc "prepare db"
  task migrate_and_seed: ["db:migrate", "db:seed"]

  task prime: ["db:migrate", "db:seed:replant"] do

    cat = Categorie.find(1)
    25.times{|i|
      Categorie.create!(nom: Faker::Lorem.sentence, sur_place:i.even?)
    }
    org = Organisation.create!(nom: "Organisation du Test (mdp test)", password: "admin", read_password: "test", categories: Categorie.all[1..-1].sample(20))
    org.categories << cat unless org.categories.include? cat
    org.annonces << org.create_annonce!(nom: "Test Organisation (mdp test)", password: "admin",
					mail: Faker::Internet.email,
					adresse: Faker::Address.full_address,
					tel1: Faker::PhoneNumber.phone_number,
					tel2: Faker::PhoneNumber.cell_phone,
					web: Faker::Internet.url,
					distance: Faker::Number.within(range: 1..50),
					autres: Faker::Lorem.paragraph,
					latlong: "48.1743,-2.1945",
					categories: [cat])

    20.times do
      ann = Annonce.create!(nom: Faker::Name.name,
			    password: "user",
			    mail: Faker::Internet.email,
			    adresse: Faker::Address.full_address,
			    tel1: Faker::PhoneNumber.phone_number,
			    tel2: Faker::PhoneNumber.cell_phone,
			    web: Faker::Internet.url,
			    distance: Faker::Number.within(range: 1..50),
			    latlong: Faker::Number.positive(from: 47.73251, to: 48.472921).to_s+","+Faker::Number.negative(from: -3.21899414, to: -1.75506591).to_s,
			    autres: Faker::Lorem.paragraph,
			    organisation: org,
			    categories: org.categories.sample(1 + rand(org.categories.count)))
    end
  end
end
