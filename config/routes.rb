Rails.application.routes.draw do
  resources :visites, only: :index
  resources :categories
  resources :annonces
  resources :organisations do
    get 'login', on: :member, action: :login_in
    get 'login', on: :collection, action: :login_form
    post 'login', on: :collection
  end
  root to: 'organisations#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
