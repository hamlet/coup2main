class AddContactsToAnnonces < ActiveRecord::Migration[6.0]
  def change
    add_column :annonces, :tel1, :string
    add_column :annonces, :tel2, :string
    add_column :annonces, :web, :string
    add_column :annonces, :mail, :string
  end
end
