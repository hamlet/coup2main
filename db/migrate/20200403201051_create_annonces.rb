class CreateAnnonces < ActiveRecord::Migration[6.0]
  def change
    create_table :annonces do |t|
      t.string :nom
      t.string :latlong
      t.integer :distance
      t.string :adresse
      t.string :password_digest
      t.references :organisation, null: false, foreign_key: true

      t.timestamps
    end
  end
end
