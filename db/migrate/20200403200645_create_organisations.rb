class CreateOrganisations < ActiveRecord::Migration[6.0]
  def change
    create_table :organisations do |t|
      t.string :nom
      t.string :password_digest
      t.string :read_password_digest

      t.timestamps
    end
  end
end
