class CreateVisites < ActiveRecord::Migration[6.0]
  def change
    create_table :visites do |t|
      t.string :path
      t.integer :count
      t.references :organisation, null: true, foreign_key: true

      t.timestamps
    end
  end
end
