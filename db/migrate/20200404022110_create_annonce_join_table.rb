class CreateAnnonceJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :annonces, :categories do |t|
      # t.index [:annonce_id, :categorie_id]
      t.index [:categorie_id, :annonce_id], unique: true, name: 'catann'
    end
  end
end
