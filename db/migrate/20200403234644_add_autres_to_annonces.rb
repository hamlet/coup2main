class AddAutresToAnnonces < ActiveRecord::Migration[6.0]
  def change
    add_column :annonces, :autres, :text
  end
end
