class CreateOrganisationJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :organisations, :categories do |t|
      # t.index [:organisation_id, :categorie_id]
      t.index [:categorie_id, :organisation_id], unique: true, name: 'catorg'
    end
  end
end
