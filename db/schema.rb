# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_11_172558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "annonces", force: :cascade do |t|
    t.string "nom"
    t.string "latlong"
    t.integer "distance"
    t.string "adresse"
    t.string "password_digest"
    t.integer "organisation_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "autres"
    t.string "tel1"
    t.string "tel2"
    t.string "web"
    t.string "mail"
    t.index ["organisation_id"], name: "index_annonces_on_organisation_id"
  end

  create_table "annonces_categories", id: false, force: :cascade do |t|
    t.integer "annonce_id", null: false
    t.integer "categorie_id", null: false
    t.index ["categorie_id", "annonce_id"], name: "catann", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "nom"
    t.boolean "sur_place"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "categories_organisations", id: false, force: :cascade do |t|
    t.integer "organisation_id", null: false
    t.integer "categorie_id", null: false
    t.index ["categorie_id", "organisation_id"], name: "catorg", unique: true
  end

  create_table "organisations", force: :cascade do |t|
    t.string "nom"
    t.string "password_digest"
    t.string "read_password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "tel1"
    t.string "tel2"
    t.string "web"
    t.string "mail"
  end

  create_table "visites", force: :cascade do |t|
    t.string "path"
    t.integer "count"
    t.bigint "organisation_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["organisation_id"], name: "index_visites_on_organisation_id"
  end

  add_foreign_key "annonces", "organisations"
  add_foreign_key "visites", "organisations"
end
