json.extract! category, :id, :nom, :sur_place, :created_at, :updated_at
json.url category_url(category, format: :json)
