json.extract! annonce, :id, :nom, :latlong, :distance, :adresse, :created_at, :updated_at
json.url annonce_url(annonce, format: :json)
