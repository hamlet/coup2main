class ApplicationController < ActionController::Base
  before_action :count_visits, only: [:index, :show]
  helper_method :current_organisation, :current_admin, :check_admin

  def current_organisation
    return unless session[:organisation_id]
    begin
      @current_organisation ||= Organisation.find(session[:organisation_id])
    rescue ActiveRecord::RecordNotFound
      reset_session
      false
    end
  end
  def current_admin
    return unless session[:admin]
    @current_admin ||= session[:admin]
  end
  def check_admin(organisation)
    current_organisation == organisation && current_admin
  end
  private

  def count_visits
    Visite.find_or_create_by!(path: request.fullpath, organisation: current_organisation).increment!(:count)
  end
end
