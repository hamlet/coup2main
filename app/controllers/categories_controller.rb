class CategoriesController < ApplicationController
  before_action :set_categorie, only: [:show, :edit, :update, :destroy]
  before_action :check_admin
  before_action :check_organisations, only: [:edit, :update, :destroy]
  before_action :check_annonces, only: [:destroy]
  before_action :keep_first_categorie, only: [:edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Categorie.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @categorie = Categorie.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @categorie = Categorie.new(categorie_params)

    respond_to do |format|
      if @categorie.save
        current_organisation&.categories << @categorie
        format.html { redirect_to @categorie, notice: 'Categorie créée avec succès.' }
        format.json { render :show, status: :created, location: @categorie }
      else
        format.html { render :new }
        format.json { render json: @categorie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @categorie.update(categorie_params)
        format.html { redirect_to @categorie, notice: 'Categorie modifiée avec succès.' }
        format.json { render :show, status: :ok, location: @categorie }
      else
        format.html { render :edit }
        format.json { render json: @categorie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @categorie.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Categorie supprimée avec succès.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_admin
      unless current_admin
        redirect_to :root, alert: "Accès réservé."
      end
    end
    # before_action :check_organisations, only: [:edit, :update, :destroy]
    def check_organisations
      unless (@categorie.organisations.count == 0) || ((@categorie.organisations.count == 1) && (@categorie.organisations.first == current_organisation))
        redirect_to Categorie, alert: "Cette catégorie est utilisée par d'autres organisations que la votre, cette opération n'est pas permise."
      end
    end
    # before_action :check_annonces, only: [:destroy]
    def check_annonces
      unless (@categorie.annonces.count == 0)
        redirect_to Categorie, alert: "Cette catégorie est utilisée par des annonces, vous ne pouvez pas la supprimer."
      end
    end
    def keep_first_categorie
      if params[:id] == "1"
        redirect_to Categorie, alert: "Cette catégorie ne peut être modifiée."
      end
    end
    def set_categorie
      @categorie = Categorie.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def categorie_params
      params.require(:categorie).permit(:nom, :sur_place)
    end
end
