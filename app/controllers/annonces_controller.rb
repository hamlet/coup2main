class AnnoncesController < ApplicationController
  before_action :check_organisation
  before_action :set_annonce, only: [:show, :edit, :update, :destroy]

  # GET /annonces
  # GET /annonces.json
  def index
    if params["filtre"] && ! params["filtre"]["categorie"].blank?
      id = params["filtre"]["categorie"].to_i
      if id >= 0
        @categorie = Categorie.find(id)
        @annonces = current_organisation.annonces.joins(:categories).includes(:categories).where(:annonces_categories => {categorie_id: id })
        if params["filtre"]["latlong"] && params["filtre"]["latlong"].length > 3
          session["latlong"] = params["filtre"]["latlong"]
          @annonces = @annonces.select do |annonce| 
            if annonce.latlong.present?
              helpers.distance(params["filtre"]["latlong"].split(',').map(&:to_f), annonce.latlong.split(',').map(&:to_f)) < annonce.distance 
            else
              false
            end
          end
        end
      else
        @categorie = Categorie.new(id: -1)
        @annonces = current_organisation.annonces.includes(:categories).where.not(autres: [nil, ""])
      end
    else
      @annonces = current_organisation.annonces.includes(:categories).all
    end
  end

  # GET /annonces/1
  # GET /annonces/1.json
  def show
  end

  # GET /annonces/new
  def new
    @annonce = Annonce.new
  end

  # GET /annonces/1/edit
  def edit
  end

  # POST /annonces
  # POST /annonces.json
  def create
    @annonce = current_organisation.annonces.new(annonce_params)

    respond_to do |format|
      if @annonce.save
        format.html { redirect_to @annonce, notice: 'Annonce créée avec succès.' }
        format.json { render :show, status: :created, location: @annonce }
      else
        format.html { render :new }
        format.json { render json: @annonce.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /annonces/1
  # PATCH/PUT /annonces/1.json
  def update
    respond_to do |format|
      if params[:annonce][:old_password] && @annonce.authenticate(params[:annonce][:old_password])
        if @annonce.update(annonce_params)
          format.html { redirect_to @annonce, notice: 'Mot de passe modifié avec succès.' }
        else
          format.html { redirect_to @annonce, alert: 'Mot de passe non autorisé.' }
        end
      else
        @annonce.assign_attributes(annonce_params.except(:password))
        if @annonce.authenticate(params[:annonce][:password])
          if @annonce.save
            format.html { redirect_to @annonce, notice: 'Annonce modifiée avec succès.' }
            format.json { render :show, status: :ok, location: @annonce }
          else
            format.html { render :edit }
            format.json { render json: @annonce.errors, status: :unprocessable_entity }
          end
        else
          flash.now[:alert] = 'Modication refusée, mot de passe incorrect.'
          format.html { render :edit }
        end
      end
    end
  end

  # DELETE /annonces/1
  # DELETE /annonces/1.json
  def destroy
    if @annonce.authenticate(params[:password])
      @annonce.destroy
      respond_to do |format|
        format.html { redirect_to annonces_url, notice: 'Annonce supprimée avec succès.' }
        format.json { head :no_content }
      end
    else
      redirect_to @annonce, alert: 'Annonce non supprimée, mot de passe incorrect.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_organisation
      unless current_organisation
        redirect_to :root, alert: "Accès réservé."
      end
    end
    def set_annonce
      @annonce = current_organisation.annonces.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def annonce_params
      params.require(:annonce).permit(:nom, :latlong, :distance, :adresse, :autres, :password, :password_confirmation, :tel1, :tel2, :web, :mail, :categorie_ids => [])
    end
end
