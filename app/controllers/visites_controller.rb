class VisitesController < ApplicationController
  before_action :check_organisation

  # GET /visites
  # GET /visites.json
  def index
    @visites = Visite.where(organisation: nil).or(Visite.where(organisation: current_organisation)).order(count: :desc)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_organisation
      unless current_organisation && current_admin
        redirect_to :root, alert: "Accès réservé."
      end
    end
end
