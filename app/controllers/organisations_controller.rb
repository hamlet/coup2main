class OrganisationsController < ApplicationController
  before_action :set_organisation, only: [:show, :edit, :update, :destroy]
  before_action :check_admin_organisation, only: [:edit, :update, :destroy]

  # GET /organisations
  # GET /organisations.json
  def index
    @organisations = Organisation.includes(:annonces).all
  end

  # GET /organisations/1
  # GET /organisations/1.json
  def show
  end

  # GET /organisations/new
  def new
    @organisation = Organisation.new
  end

  # GET /organisations/1/edit
  def edit
  end

  # GET /organisations/login
  def login_form
    @organisations = Organisation.all
  end

  # GET /organisations/1/login
  def login_in
    @organisations = [Organisation.find(params[:id])]
    render :login_form
  end

  # POST /organisations/login
  def login
    reset_session
    @organisation = Organisation.find(params[:organisation][:id])
    if @organisation.authenticate(params[:organisation][:password])
      session[:organisation_id] = @organisation.id
      session[:admin] = true
      redirect_to Annonce, notice: "Authentifié avec succès en tant qu'admin !"
    elsif @organisation.authenticate_read_password(params[:organisation][:password])
      session[:organisation_id] = @organisation.id
      session[:admin] = false
      redirect_to Annonce, notice: 'Authentifié avec succès !'
    else 
      @organisations = [@organisation]
      flash.now[:alert] = "Mot de passe incorrect"
      render :login_form
    end
  end

  # POST /organisations
  # POST /organisations.json
  def create
    @organisation = Organisation.new(organisation_params)

    respond_to do |format|
      if @organisation.save
        reset_session
        session[:organisation_id] = @organisation.id
        session[:admin] = true
        a = @organisation.build_annonce(nom: @organisation.nom, password: @organisation.password)
        a.save(validate: false)
        @organisation.annonces << a
        @organisation.annonce.categories << Categorie.find(1)
        @organisation.categories << Categorie.find(1) unless @organisation.categories.exists?(1)
        format.html { redirect_to edit_annonce_url(@organisation.annonce), notice: 'Organisation créée avec succès.' }
        format.json { render :show, status: :created, location: @organisation }
      else
        format.html { render :new }
        format.json { render json: @organisation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organisations/1
  # PATCH/PUT /organisations/1.json
  def update
    respond_to do |format|
      if @organisation.update(organisation_params)
        format.html { redirect_to @organisation, notice: 'Organisation modifiée avec succès.' }
        format.json { render :show, status: :ok, location: @organisation }
      else
        format.html { render :edit }
        format.json { render json: @organisation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organisations/1
  # DELETE /organisations/1.json
  def destroy
    if @organisation.authenticate(params[:password])
      @organisation.destroy
      respond_to do |format|
        format.html { redirect_to organisations_url, notice: 'Organisation supprimée avec succès.' }
        format.json { head :no_content }
      end
    else
      redirect_to @organisation, alert: 'Organisation non supprimée, mot de passe incorrect.' 
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_organisation
    @organisation = Organisation.find(params[:id])
  end
  def check_admin_organisation
    unless check_admin(@organisation)
      redirect_to :root, alert: "Accès réservé."
    end
  end

  # Only allow a list of trusted parameters through.
  def organisation_params
    params.require(:organisation).permit(:nom, :password, :password_confirmation, :read_password, :read_password_confirmation, :categorie_ids => [])
  end
end
