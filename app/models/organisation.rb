class Organisation < ApplicationRecord
  has_secure_password
  has_secure_password :read_password
  validates :nom, presence: true, length: { minimum: 3 }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 3 }, on: :create
  validates :read_password, presence: true, length: { minimum: 3 }, on: :create

  has_many :annonces, dependent: :destroy
  has_one :annonce, -> { order('created_at asc').limit(1) }, dependent: :destroy
  has_and_belongs_to_many :categories

  after_update :update_annonce

  private
  def update_annonce
    self.annonce&.update_attribute(:nom, self.nom)
  end
end
