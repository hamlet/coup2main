class Categorie < ApplicationRecord
  default_scope { order(nom: :asc) }
  validates :nom, presence: true, length: { minimum: 4, maximum: 110 }
  has_and_belongs_to_many :organisations
  has_and_belongs_to_many :annonces
  def name
    (id == -1) ? self.nom : self.nom + (self.sur_place ? ", sur place" : ", à distance")
  end
end
