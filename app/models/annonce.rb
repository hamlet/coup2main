class Annonce < ApplicationRecord
  has_secure_password
  validates :nom, presence: true, length: { minimum: 3 }, uniqueness: { case_sensitive: false, scope: :organisation }
  validates :password, presence: true, length: { minimum: 3 }, on: :create
  validates :mail, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, if: :mail? 
  validates :tel1, format: { with: /\A((\+)33\s?|0)[1-9]\s?(\d{2}\s?){4}\z/ }, if: :tel1? # +33299876554 ou 06 17 62 17 71
  validates :tel2, format: { with: /\A((\+)33\s?|0)[1-9]\s?(\d{2}\s?){4}\z/ }, if: :tel2? # +33299876554 ou 06 17 62 17 71
  validates :distance, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, presence: true, if: :latlong?
  validates :latlong, format: { with: /\A-?\d+(\.\d+)?,-?\d+(\.\d+)?\z/ }, if: :latlong? # -22.78,-43.77 
  validates :categories, presence: true
  validate :any_contact_present?
  validate :password_different_from_org

  def any_contact_present?
    if %w(mail tel1 tel2 web).all?{|attr| self[attr].blank?}
      errors.add :base, "Au moins un moyen de contact doit être rempli"
    end
  end
  def password_different_from_org
    if self.organisation.authenticate_read_password(password) || self.organisation.authenticate(password)
      errors.add :password, "ne doit pas être le même que pour l'accès aux annonces."
    end
  end

  belongs_to :organisation
  has_and_belongs_to_many :categories
  def web
    _web = read_attribute(:web).try(:downcase)
    if(_web.present?)
      unless _web[/\Ahttp:\/\//] || _web[/\Ahttps:\/\//]
        _web = "http://#{_web}"
      end
    end
    _web
  end
end
