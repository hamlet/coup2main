import "leaflet";
import 'leaflet/dist/leaflet.css';
import 'leaflet-control-geocoder';
import 'leaflet-control-geocoder/dist/Control.Geocoder.css';
import 'leaflet-gps';
import 'leaflet-gps/dist/leaflet-gps.min.css';

/* This code is needed to properly load the images in the Leaflet CSS */
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

document.addEventListener("turbolinks:load", () => {
  var macarte = null;
  var marker, circle, geoc;
  $('#mapModal').on("shown.bs.modal", function (e) {
    if (macarte == null) {
      macarte = L.map('map');
      var latlong = [0,0];
      var form = false;
      var list = false;
      var bounds = new L.LatLngBounds();
      if($('[data-map]').length && $('[data-map]').data('map') != ""){
        if($('[data-map]').length > 1) { // page d'accueil
          $('[data-map]').each(function( i ) {
            latlong = $(this).data('map').split(',');
            bounds.extend(L.marker(latlong).addTo(macarte).bindPopup($(this).data('text')).getLatLng());
          })
          list = true
        } else {
          latlong = $('[data-map]').data('map').split(',');
        } 
      } 
      if($("#annonce_latlong").length || $("#filtre_latlong").length){ // If form
        geoc = L.Control.geocoder({
          defaultMarkGeocode: false,
          position: 'topleft'
        }).on('markgeocode', function(e) {
          var center = e.geocode.center;
          macarte.setView(center);
          marker.setLatLng(center).fire('drag');
        }).addTo(macarte);
        if($("#annonce_adresse").length){
          geoc.setQuery($("#annonce_adresse").val())
        }
        if($("#annonce_latlong").length && $("#annonce_latlong").val() != "") {
          latlong = $("#annonce_latlong").val().split(',');
        }else if($("#filtre_latlong").length && $("#filtre_latlong").val() != "") {
          latlong = $("#filtre_latlong").val().split(',');
        }
        form = true;
      }
      if($("#annonce_distance").length) {
        circle = L.circle(latlong, {radius: ($("#annonce_distance").val() * 1000), fill: false}).addTo(macarte);
      }
      // Fonction d'initialisation de la carte
      // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
      // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
      L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="https://www.openstreetmap.org/copyright">OpenStreetMap/ODbL</a> - rendu <a href="https://www.openstreetmap.fr/">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
      }).addTo(macarte);
      L.control.scale().addTo(macarte);
      if (! list) {
        marker = L.marker(latlong,{draggable: form, autoPan: true}).addTo(macarte);
        if(form) {
          marker.on("drag", function(e) {
            var position = e.target.getLatLng();
            if($("#annonce_latlong").length){
              $("#annonce_latlong").val([position.lat, position.lng].join());
            } else if ($("#filtre_latlong").length){ 
              $("#filtre_latlong").val([position.lat, position.lng].join());
            }
            (typeof circle !== 'undefined') && circle.setLatLng(position);
          })
          macarte.on('dblclick', function(e){
            var center = e.latlng;
            macarte.setView(center);
            marker.setLatLng(center).fire('drag');
          });
          new L.Control.Gps().on('gps:located', function(e) {
            var center = e.latlng;
            macarte.setView(center);
            marker.setLatLng(center).fire('drag');
          }).addTo(macarte);
        }
        macarte.setView(latlong, 15);
        if((typeof circle !== 'undefined') && (circle.getRadius() > 0)){
          macarte.fitBounds(circle.getBounds());
        }
      } else { // liste, page d'accueil
        macarte.fitBounds(bounds);
      }
    } else { // carte déjà initialisée
      (typeof circle !== 'undefined') && $("#annonce_distance").length && circle.setRadius($("#annonce_distance").val() * 1000) && (circle.getRadius() > 0) && macarte.fitBounds(circle.getBounds());
      if((typeof geoc !== 'undefined') && $("#annonce_adresse").length){
        geoc.setQuery($("#annonce_adresse").val())
      }
    }
  })
})
