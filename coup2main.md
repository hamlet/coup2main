# Coup2Main

Application de mise en relation de voisins pour le partage de coup de main, à distance ou sur place.
Administré par un collectif global (associations, collectivités)

## Modèle de données

### Organisation

#### Liste 

Affiche la liste des organisations de l'instance

#### Créer

Créer une nouvelle organisation.

* Nom
* Mot de passe admin
* Mot de passe utilisateur
* Liste des catégories proposables

#### Voir

Liste les infos de contacts.

#### Modifier / supprimer

Avec le mot de passe admin, modifier les infos, ou supprimer l'organisation avec toutes les fiches associées.

### Annonce

#### Liste 

Après vérification du mot de passe utilisateur de l'organisation, affiche les annonces liées.

Filtrage par catégorie.

Après renseignement d'un lieu (soit par adresse, plus raffinage sur carte, soit par partage de position de smartphone), filtrage en fonction de la distance proposée et affichage sur carte.

#### Créer

Créé une nouvelle annonce.

* Nom
* Contacts (site web, mail, téléphone(s))
* Localisation (par adresse puis précision sur carte, ou localisatoin de smartphone)
* Distance acceptable pour les catégories nécessitant un déplacement.
* Catégories proposées.
* Texte libre mis en forme via Markdown
* Mot de passe

#### Voir

Affiche tous les détails de l'annonce.

### Catégories

#### Créer

Avec vérification d'un mot de passe admin d'organisation.

Ajoute une nouvelle catégorie (Nom + déplacement?).
