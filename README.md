# README

Application de mise en relation de voisins pour le partage de coup de main, à distance ou sur place.
Administré par un collectif global (associations, collectivités).

Plus de détails dans le [cahier des charges](coup2main.md)...

## Versions en ligne

Une version de test est accessible à <https://coup2main-demo.herokuapp.com/>. Les données saisies seront effacées régulièrement.

La version de production est <https://coup2main.herokuapp.com/>. Merci de la garder propre.

## Informations techniques

* Ruby on Rails 6.0
* Ruby 2.5.5
* Webpack
* Leaflet

## Développement

Si vous voulez aider à corriger ou améliorer ce projet, n'hésitez pas la fourcher, puis à proposer des "merge request".

Cette application est conçue pour un usage précis. Si certains choix ne vous conviennent pas, n'hésitez pas à la modifiez et à héberger votre propre version.

## Installation

Normalement l'installation locale est aussi simple que (pas encore testé) :

```bash
git clone https://gitlab.com/hamlet/coup2main.git
buncle install # --path vendor/bundle pour garder les gems dans le répertoire courant
rails db:migrate
rails db:seed # Créé les données indispensables
# rails dev:prime # Créé tout un tas de données crédibles pour tester
```

La configuration par défaut utilise pgsql. Il faut modifier `databases.yml` pour revenir à sqlite.

## Déploiement

La config est prévue pour un déploiement vers Heroku. Il suffit de suivre le [tuto](https://devcenter.heroku.com/articles/getting-started-with-rails6).

## Tests

Malheureusement pas de tests automatiques, il faut le faire à la main.
